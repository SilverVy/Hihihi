﻿import json
import sys
import os


def add_to_dict():
    """
    Добавляет в словарь новые
    """
    dictionary = reader()
    count = 0
    try:
        count_roads = int(input('Пожалуйста, введите количество' \
                                ' сотовых телефонов,' \
                                ' которое вы хотите добавить: '))
    except ValueError:
        print('Пожалуйста, введите корректные данные!')
        count_roads = int(input('Пожалуйста, количество введите сотовых' \
                                ' телефонов, которое вы хотите добавить: '))
    print('Введите данные о сотовом(ых) ' \
          'телефоне(ах) в формате: марка:' \
          ' модель год выпуска ' \
          'цена')
    for _ in range(count_roads):
        try:
            key, value = input('Введите данные о ' \
                               'сотовом телефоне: ').split(': ')
            value = value.split()
            value[2] = int(value[2])
            value[1] = int(value[1])
        except ValueError:
            print('Пожалуйста, введите корректные данные')
            key, value = input('Введите данные о ' \
                               'сотовом телефоне: ').split(': ')
            value = value.split()
            value[2] = int(value[2])
            value[1] = int(value[1])
        if key not in dictionary:
            dictionary[key] = value
            count += 1
        else:
            print('Такой телефон уже есть')
        writer(dictionary)
        if count == 0:
            return 'Новых телефонов не было добавлено!'
        if count == 1:
            return 'Файл успещно добавлен!'
        return 'Файлы успешно добавлены!'



def writer(dictionary: dict):
    """
    Записывает словарь в файл
    """
    with open('input.json', 'w', encoding="utf-8") as file:
        json.dump(dictionary, file)


def reader():
    """
    Запись словаря из файла в переменную
    """

    with open('input.json', encoding="utf-8") as file:
        if os.path.getsize('input.json') == 0:
            buf = {}
            writer(buf)
        dictionary = json.load(file)
    return dictionary


def rem_from_dict():
    """
    Удаляет сотовый телефон из словаря
    """
    dictionary = reader()
    item = input(f'Полный список телефнов: {list(dictionary.keys())}.'
                 f'\nВведите название сотового ' \
                 f'телефона, который вы бы хотели удалить: ')
    if item in dictionary:
        del dictionary[item]
    else:
        return 'Такого телефона и так нет на складе'
    writer(dictionary)
    return 'Телефон успешно удалён'


def search():
    """
    Ищет сотовый телефон по названию и возвращает данные о нём
    """
    dictionary = reader()
    need = input(f'Полный список сотовых' \
                 f' телефонов: {[value[0] for key, value in dictionary.items()]}.'
                 f'\nВведите название сотового телефона' \
                 f', о котором вы бы хотели узнать: ')
    for key, value in dictionary.items():
        if need == value[0]:
            return f'Марка: {key}, Модель: {value[0]}, Год выпуска: ' \
                   f'{value[1]}, Цена' \
                   f': {value[2]}'
    return 'Такого сотового телефона нет'


def filter_dict():
    """
    Фильтрует по продолжительности пути
    """
    perem = input('Пожалуйста, введите по какоmу значению фильтровать данные:'
              '{оператор сравнения} {значение}: ')
    if '=' not in perem and '>' not in perem and '<' not in perem:
        print('Пожалуйста, введите корректные данные!')
        perem = input('Форmат: {оператор сравнения} {значение}: ')
    command, need = perem.split()
    try:
        need = int(need)
    except ValueError:
        print('Введите корректные данные!')
        perem = input('{оператор сравнения} {значение}: ')
        command, need = perem.split()
        need = int(need)
    dictionary = reader()

    def func_compare(value, command, need):
        """
        Проверяет, подходит ли данная цена по заданному условию
        """
        if command == '<':
            return value < need
        if command == '>':
            return value > need
        if command == '=':
            return value == need
        return None

    sort_dict = {}
    for key, value in dictionary.items():
        if func_compare(value[2], command, need):
            sort_dict[key] = value

    sort_dict = dict(sorted\
                     (sort_dict.items(), key=lambda\
                         x: x[1][2], reverse=True))

    return sort_dict


def output_dict(dictionary):
    """
    Выводит все сотовые телефоны и данные о них
    """
    list_output = []
    for key, value in dictionary.items():
        list_output.append(
            f'Марка: {key}, Модель' \
            f': {value[0]}, Год выпуска:' \
            f' {value[1]}, Цена' \
            f': {value[2]}')
    return '\n'.join(list_output)


def menu():
    """
    Запускает программу, оперируя необходимыми функциями
    """
    print('Выберите желаемое действе:\n'
          'Введите 1, если хотите вывести информацию' \
          ' обо всех сотовых телефонах;\n'
          'Введите 2, если хотите выполнить поиск'
          ' сотового телефона по наименованию;\n' \
          'Введите 3, если хотите фильтровать сотовые' \
          ' телефоны по цене;\n'
          'Введите 4, если хотите добавить позиции;\n'
          'Введите 5, если хотите удалить позиции;\n'
          'Введите 0, если хотите завершить программу;\n'
          )
    vvod = input('Введите желаемое действие: ')
    if vvod == '1':
        print(output_dict(reader()))
    elif vvod == '2':
        print(search())
    elif vvod == '3':
        print(output_dict(filter_dict()))
    elif vvod == '4':
        print(add_to_dict())
    elif vvod == '5':
        print(rem_from_dict())
    elif vvod == '0':
        sys.exit()


if __name__ == '__main__':
    print('Программа реализует хранение информации об сотовых телефонах')
    print("")
    while True:
        menu()
        print('')